class CreateSeries < ActiveRecord::Migration[6.0]
  def up
    create_table :series do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
  def down
    drop_table :series
  end
end
