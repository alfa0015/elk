# frozen_string_literal: true
Doorkeeper.configure do
  orm :active_record
  resource_owner_from_credentials do |_routes|
    user = User.find_for_database_authentication(email: params[:email])
    if user&.valid_for_authentication? { user.valid_password?(params[:password]) }
      user
    end
  end
  grant_flows %w(password)
  access_token_expires_in 120.hours
  use_refresh_token
  skip_authorization do
    true
  end
end
