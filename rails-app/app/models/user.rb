class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, :password_confirmation, presence: true
  validates :email, uniqueness: true
  validates :password, :password_confirmation, length: { minimum: 8 }

  has_many :tokens,
           class_name: 'Doorkeeper::AccessToken',
           foreign_key: :resource_owner_id, inverse_of: false,
           dependent: :destroy
  has_one :token,
          -> { order 'created_at DESC' },
          class_name: 'Doorkeeper::AccessToken',
          foreign_key: :resource_owner_id,
          inverse_of: false,
          dependent: :destroy
end