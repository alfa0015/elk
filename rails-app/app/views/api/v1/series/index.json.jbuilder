json.data do
	json.array! @series do |serie|
		json.id serie.id
		json.name serie.name
	end
end