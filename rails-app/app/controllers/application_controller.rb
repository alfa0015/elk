class ApplicationController < ActionController::API
    before_action :set_locale
  
    def set_locale
      I18n.locale = params[:locale] || I18n.default_locale
    end
  
    def default_url_options
      { locale: I18n.locale }
    end
  
    rescue_from ActiveRecord::RecordNotFound do |exception|
      render json: { errors: exception }, status: :not_found
    end
  
    def current_resource_owner
      User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
    end
    
  end
  