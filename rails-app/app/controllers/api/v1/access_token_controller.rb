# frozen_string_literal: true

# Clase custom AccessToken Doorkeeper
module Api
    module V1
      class AccessTokenController < Doorkeeper::TokensController
      end
    end
  end
  