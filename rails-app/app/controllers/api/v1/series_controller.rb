# frozen_string_literal: true

module Api
    module V1
      # Clase custom para registro de devise
      class SeriesController < ApplicationController
        def index
            @series = Serie.all.order(id: :desc)
        end
  
        private
  
        def series_params
          params.permit(:email, :password, :password_confirmation)
        end
      end
    end
  end
  