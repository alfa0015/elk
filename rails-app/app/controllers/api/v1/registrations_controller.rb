# frozen_string_literal: true

module Api
    module V1
      # Clase custom para registro de devise
      class RegistrationsController < Devise::RegistrationsController
        respond_to :json
  
        def create
          build_resource(sign_up_params)
          if resource.save
            respond_with resource, location: after_sign_up_path_for(resource)
          else
            respond_with resource, location: after_inactive_sign_up_path_for(resource)
          end
        end
  
        private
  
        def sign_up_params
          params.permit(:email, :password, :password_confirmation)
        end
      end
    end
  end
  