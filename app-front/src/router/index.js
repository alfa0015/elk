import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import { ApmVuePlugin } from '@elastic/apm-rum-vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

Vue.use(ApmVuePlugin, {
  router,
  config: {
    serviceName: `${process.env.VUE_APP_APM_SERVICE_NAME}`,
    secretToken: `${process.env.VUE_APP_APM_TOKEN}`,
    serverUrl: `${process.env.VUE_APP_APM_SERVER_URL}`
    // agent configuration
  }
})

export default router
